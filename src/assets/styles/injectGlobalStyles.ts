const injectGlobalStyles = () => {

    const TT_Norms_Pro_ExtraBold = require('../typography/TT_Norms_Pro_ExtraBold.woff2');
    const TT_Norms_Pro_Medium = require('../typography/TT_Norms_Pro_Medium.woff2');
    const Lato_Bold = require('../typography/Lato-Bold.woff2');
    const Lato_Medium = require('../typography/Lato-Medium.woff2');
    const Lato_Regular = require('../typography/Lato-Regular.woff2');
    const Lato_Semibold = require('../typography/Lato-Semibold.woff2');

    return `
    body {
      padding: 0;
      margin: 0;
    }
    
@font-face {
    font-family: 'TT Norms Pro', sans-serif;
    src: url(${TT_Norms_Pro_ExtraBold}) format('woff2');
    font-weight: bold;
    font-display: swap;
    font-style: normal;
}

@font-face {
    font-family: 'TT Norms Pro', sans-serif;
    src: url(${TT_Norms_Pro_Medium}) format('woff2');
    font-weight: 500;
    font-display: swap;
    font-style: normal;
}

@font-face {
    font-family: 'Lato', sans-serif;
    src: url(${Lato_Bold}) format('woff2');
    font-weight: bold;
    font-display: swap;
    font-style: normal;
}

@font-face {
    font-family: 'Lato', sans-serif;
    src: url(${Lato_Medium}) format('woff2');
    font-weight: 500;
    font-display: swap;
    font-style: normal;
}

@font-face {
    font-family: 'Lato', sans-serif;
    src: url(${Lato_Regular}) format('woff2');
    font-weight: normal;
    font-display: swap;
    font-style: normal;
}

@font-face {
    font-family: 'Lato', sans-serif;
    src: url(${Lato_Semibold}) format('woff2');
    font-weight: 600;
    font-display: swap;
    font-style: normal;
}
  `;
};

export default injectGlobalStyles;