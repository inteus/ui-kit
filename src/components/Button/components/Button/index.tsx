import React, {forwardRef, ReactElement} from 'react';
import * as S from './styled';
import {ButtonVariant} from '../../types';

export interface ButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  loading?: boolean;
  growWidth?: boolean;
  variant?: ButtonVariant;
}

export const Button = forwardRef<HTMLButtonElement, ButtonProps>(
  (
    {
      loading = false,
      children,
      className,
      disabled = false,
      type = 'button',
      growWidth = false,
      variant = ButtonVariant.MediumPrimary,
      ...otherProps
    }: ButtonProps,
    forwardedRef,
  ): ReactElement => {
    return (
      <S.Button
        ref={forwardedRef}
        $variant={variant}
        $growWidth={growWidth}
        $loading={loading}
        className={className}
        type={type}
        disabled={disabled || loading}
        {...otherProps}
      >
        {loading && <S.Loader variant={variant} />}
        <S.Inner $variant={variant} $loading={loading}>
          {children}
        </S.Inner>
      </S.Button>
    );
  },
);
