import styled, {css, FlattenSimpleInterpolation} from 'styled-components';
import {ButtonVariant} from '../../types';
import {Spinner} from '../../../Spinner';

const buttonStyle: Record<
  ButtonVariant,
  {
    normal: FlattenSimpleInterpolation;
    hover: FlattenSimpleInterpolation;
    loading: FlattenSimpleInterpolation;
    disabled: FlattenSimpleInterpolation;
  }
> = {
  [ButtonVariant.LargePrimary]: {
    normal: css`
      font-size: 14px;
      line-height: 24px;
      background-color: var(--color-blue-primary);
      color: var(--color-white);
      padding: 4px 16px;
      height: 32px;
      gap: 10px;
    `,
    hover: css`
      background-color: var(--color-blue-dark);
    `,
    loading: css`
      background-color: var(--color-blue-dark);
    `,
    disabled: css`
      background-color: var(--color-blue-extra-light);
    `,
  },
  [ButtonVariant.MediumPrimary]: {
    normal: css`
      padding: 4px 16px;
      font-size: 14px;
      line-height: 24px;
      color: var(--color-white);
      background-color: var(--color-blue-10);
      height: 32px;
    `,
    hover: css`
      background-color: var(--color-blue-11);
    `,
    disabled: css`
      background-color: var(--color-blue-5);
    `,
    loading: css`
      background-color: var(--color-blue-5);
    `,
  },
  [ButtonVariant.MediumSecondary]: {
    normal: css`
      padding: 4px 16px;
      font-size: 14px;
      line-height: 24px;
      color: var(--color-black);
      background-color: var(--color-blue-1);
      height: 32px;
    `,
    hover: css`
      background-color: var(--color-blue-2);
      color: var(--color-black);
    `,
    disabled: css`
      background-color: var(--color-blue-1);
      color: var(--color-blue-5);
    `,
    loading: css`
      background-color: var(--color-blue-1);
      color: var(--color-blue-5);
    `,
  },
  [ButtonVariant.MediumDanger]: {
    normal: css`
      padding: 4px 16px;
      font-size: 14px;
      line-height: 24px;
      color: var(--color-white);
      background-color: var(--color-red-11);
      height: 32px;
    `,
    hover: css`
      background-color: var(--color-red-12);
      color: var(--color-white);
    `,
    disabled: css`
      background-color: var(--color-red-3);
      color: var(--color-white);
    `,
    loading: css`
      background-color: var(--color-red-3);
      color: var(--color-white);
    `,
  },
  [ButtonVariant.MediumGhost]: {
    normal: css`
      padding: 4px 16px;
      font-size: 14px;
      line-height: 24px;
      color: var(--color-secondary);
      height: 32px;
    `,
    hover: css`
      background-color: var(--color-blue-1);
      color: var(--color-secondary);
    `,
    disabled: css`
      color: var(--color-blue-3);
    `,
    loading: css`
      color: var(--color-secondary);
    `,
  },
};

const spinnerStyle: Record<ButtonVariant, FlattenSimpleInterpolation> = {
  [ButtonVariant.LargePrimary]: css`
    font-size: 8px;
  `,
  [ButtonVariant.MediumPrimary]: css`
    font-size: 8px;
    border-color: var(--color-white);
    border-width: 1px;
    position: absolute;
  `,
  [ButtonVariant.MediumSecondary]: css`
    font-size: 8px;
    border-color: var(--color-blue-5);
    border-width: 1px;
    position: absolute;
  `,
  [ButtonVariant.MediumDanger]: css`
    font-size: 8px;
    border-color: var(--color-white);
    border-width: 1px;
    position: absolute;
  `,
  [ButtonVariant.MediumGhost]: css`
    font-size: 8px;
    border-color: var(--color-secondary);
    border-width: 1px;
    position: absolute;
  `,
};

export const Inner = styled.div<{$loading: boolean; $variant: ButtonVariant}>`
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
  visibility: ${(p) =>
    p.$loading && p.$variant !== ButtonVariant.LargePrimary ? 'hidden' : 'visible'};
`;

export const Button = styled.button<{
  disabled: boolean;
  $growWidth: boolean;
  $variant: ButtonVariant;
  $loading: boolean;
}>`
  ${(p) => buttonStyle[p.$variant].normal};
  flex-shrink: ${(p) => (p.$growWidth ? 1 : 0)};
  width: ${(p) => (p.$growWidth ? '100%' : 'auto')};
  position: relative;
  border-radius: 3px;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: ${(p) => (p.disabled ? 'not-allowed' : 'pointer')};
  transition: all 0.25s ease;

  &:hover:enabled {
    ${(p) => buttonStyle[p.$variant].hover};
  }
  &:active:enabled {
    ${Inner} {
      transform: translateY(1px);
    }
  }
  &:disabled {
    ${(p) => (p.$loading ? buttonStyle[p.$variant].loading : buttonStyle[p.$variant].disabled)};
  }
`;

export const Loader = styled(Spinner)<{variant: ButtonVariant}>`
  ${(p) => spinnerStyle[p.variant]};
  border-left-color: transparent;
`;
