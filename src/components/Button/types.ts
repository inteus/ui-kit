export enum ButtonVariant {
  LargePrimary,
  MediumPrimary,
  MediumSecondary,
  MediumDanger,
  MediumGhost,
}
