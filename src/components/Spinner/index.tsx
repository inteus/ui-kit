import * as S from './styled';

interface Props {
  className?: string;
  radius?: number;
  color?: string;
  thickness?: number;
}

export function Spinner({radius = 12, color, thickness = 2, className}: Props) {
  return <S.Spinner thickness={thickness} radius={radius} color={color} className={className} />;
}
