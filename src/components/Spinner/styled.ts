import styled, {css, keyframes} from 'styled-components';

const rotateKeyframe = keyframes`
  to {
    transform: rotate(360deg);
  }
`;

export const Spinner = styled.span<{
  radius: number;
  color?: string;
  thickness: number;
}>`
  display: block;
  box-sizing: border-box;
  min-width: 2em;
  max-width: 2em;
  min-height: 2em;
  max-height: 2em;
  font-size: ${(p) => p.radius}px;
  border: ${(p) => p.thickness}px solid ${(p) => p.color || css`var(--color-white)`};
  border-left-color: transparent;
  border-radius: 50%;
  animation: ${rotateKeyframe} 0.9s ease infinite;
`;
